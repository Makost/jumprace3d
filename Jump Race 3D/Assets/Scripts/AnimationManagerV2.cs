﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManagerV2 : MonoBehaviour
{
    private Animator _animator;
    //private const string StateKey = "AnimationState";
    private const string StateKey = "State";
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        try
        {
            if (_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name == "Flip")
            {
                _animator.SetBool("isBounced",false);
            }
        }
        catch (Exception e)
        {
            // ignored
        }
    }

    public void Bounce()
    {
        _animator.SetInteger(StateKey,1);
        _animator.SetBool("isBounced",true);
    }
    public void Fly()
    {
        _animator.SetInteger(StateKey,0);
    }

    public void Win()
    {
        _animator.SetInteger(StateKey,2);
    }
}
