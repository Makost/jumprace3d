﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip winSound;
    [SerializeField] private AudioClip waterSound;
    [SerializeField] private AudioClip bounceSound;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayWinSound()
    {
        audioSource.PlayOneShot(winSound);
    }

    public void PlayBounceSound()
    {
        audioSource.PlayOneShot(bounceSound);
    }

    public void PlayWaterSound()
    {
        audioSource.PlayOneShot(waterSound);
    }
}
