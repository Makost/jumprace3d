﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    [SerializeField] private Transform spriteMaskParent;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private GameObject pointer;
    private float startHeight = 0.8f;

    public void ChangeSpriteMaskHeight(Vector3 point,Vector3 startPoint)
    {
        var scale = spriteMaskParent.localScale;
        spriteMaskParent.localScale = new Vector3(scale.x,Mathf.Abs(startPoint.y+0.5f-point.y)/startHeight,scale.z);
        spriteMaskParent.position = startPoint+new Vector3(0,1f,0);
         pointer.transform.position = point+new Vector3(0,0.1f,0);
         sprite.transform.position = new Vector3(startPoint.x,sprite.transform.position.y,startPoint.z);
        //Debug.DrawLine(transform.position,transform.position+new Vector3(0,height,0),Color.magenta);
        //Debug.Log("1");
    }

    public void ChangeSpriteMaskColor(bool isRed)
    {
        sprite.color = isRed ? new Color(1,0,0,0.1f) : new Color(0,1,0,0.13f);
    }
}
