﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Indicator indicator;
    public bool isMoving;
    private bool isWon;
    private const float startDelay = 0.3f;
    private float delayTimer = 0f;
    private GameObject lastPlatform;
    private AnimationManagerV2 _animationManager;
    private bool isReady;


    private void Start()
    {
        delayTimer = startDelay;
        _animationManager = GetComponent<AnimationManagerV2>();
    }

    void FixedUpdate()
    {
        if (!isWon)
        {
            CheckAbovePlatform();
            if (isMoving)
            {
                //Moving player forward
                Move();
            }
        }
    }

    public void CheckAbovePlatform()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up * 0.3f, transform.TransformDirection(Vector3.down), out hit,
            Mathf.Infinity))
        {
            //Delaying platform checking to prevent falling on edge
            delayTimer -= Time.deltaTime;
            if (delayTimer <= 0)
            {
                isReady = true;
                delayTimer = startDelay;
            }

            if (hit.collider.gameObject.CompareTag("Platform"))
            {
                if (hit.collider.gameObject != lastPlatform && isReady)
                {
                    isMoving = false;
                    lastPlatform = hit.collider.gameObject;
                }
                indicator.ChangeSpriteMaskColor(false);
              //  _animationManager.Fly();
            }
            //Check for being above win block
            else if (hit.collider.gameObject.CompareTag("Win"))
            {
                if (isReady)
                {
                    isWon = true;
                }
                indicator.ChangeSpriteMaskColor(false);
            }
            else
            {
                if (isReady)
                {
                    lastPlatform = null;
                    isMoving = true;
                }
                indicator.ChangeSpriteMaskColor(true);
            }

            isReady = false;
            
           
            
            indicator.ChangeSpriteMaskHeight(hit.point,transform.position);
        }
    }

    private void Move()
    {
        // GetComponent<Rigidbody>().AddForce(Time.fixedDeltaTime * speed *transform.forward);
        transform.Translate(Time.deltaTime * speed * Vector3.forward);
    }
}