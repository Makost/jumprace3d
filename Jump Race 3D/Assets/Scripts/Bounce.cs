﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bounce : MonoBehaviour
{
    [SerializeField] private SoundPlayer soundPlayer;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private ParticleManager particleManager;
    [SerializeField] private UIManager uiManager;
    [SerializeField] private ProgressManager progressManager;
    [SerializeField] private TextEffectManager textEffectManager;
    private AnimationManagerV2 _animationManager;
    public bool isWon;

    private void Start()
    {
        _animationManager = GetComponent<AnimationManagerV2>();
    }

    private void OnCollisionEnter(Collision other)
    {
        progressManager.IncreaseProgress(other.gameObject);
        if (other.gameObject.CompareTag("Platform"))
        {
            //Bouncing character up
            BounceCharacter();
            soundPlayer.PlayBounceSound();
            if(Random.Range(0,3)==0){textEffectManager.ShowEffect(Random.Range(0,3));}
            
            
        }
        if (other.gameObject.CompareTag("Win") && !isWon)
        {
            soundPlayer.PlayWinSound();
            isWon = true;
            GetComponent<Rigidbody>().isKinematic = true;
            _animationManager.Win();
            uiManager.DisplayNextLevel();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            soundPlayer.PlayWaterSound();
            cameraTransform.parent = null;
            particleManager.PlayWaterParticles();
            uiManager.DisplayRestartLevel();
        }
    }

    private void BounceCharacter()
    {
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * 10;
        GetComponent<MoveForward>().isMoving = true;
        _animationManager.Bounce();
    }
}
