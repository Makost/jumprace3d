﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    private Animator _animator;
    //private const string StateKey = "AnimationState";
    private const string StateKey = "State";
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void Fly()
    {
        _animator.SetInteger(StateKey,1);
    }

    public void Bounce()
    {
        _animator.SetInteger(StateKey,2);
    }

    public void Win()
    {
        _animator.SetInteger(StateKey,3);
    }
}
