﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    [SerializeField] private List<ParticleSystem> waterParticles;

    public void PlayWaterParticles()
    {
        //Playing all water particle systems
        waterParticles.ForEach(x=>x.Play());
    }
}
