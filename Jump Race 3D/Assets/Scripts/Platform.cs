﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private GameObject crushedPlatform;
    [SerializeField] private bool isCrushable = false;
    private bool isCrushed = false;

    private void OnCollisionEnter(Collision other)
    {
        if (isCrushable && !isCrushed)
        {
            //Creating crushed platform with offset;
            var newPosition = transform.position;
            Instantiate(crushedPlatform, newPosition, Quaternion.identity);
            isCrushed = true;
            Destroy(gameObject);
        }
    }
}