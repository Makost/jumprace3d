﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private GameObject buttonPanel;
    [SerializeField] private RectTransform progressBar;
    [SerializeField] private float startLength;
    [SerializeField] private float endLength;
    public void RestartLevel()
    {
        levelManager.RestartLevel();
    }

    public void DisplayRestartLevel()
    {
        buttonPanel.GetComponent<Animation>().Play("restart");
    }

    public void NextLevel()
    {
        levelManager.GoToNextLevel();
    }

    public void DisplayNextLevel()
    {
        if (!levelManager.IsMaxLevel())
        {
            buttonPanel.GetComponent<Animation>().Play("nextLevel");
        }
    }

    public void IncreaseProgressBar(float state)
    {
        
        progressBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,Mathf.Lerp(startLength,endLength,state));
    }
}
