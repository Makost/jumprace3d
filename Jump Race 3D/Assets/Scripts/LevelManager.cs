﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static int currentIndex;
    private LevelManager Instance;

    private void Start()
    {
       //Preventing Level Manager from destroying or duplicating
       DontDestroy();
       //currentIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public bool IsMaxLevel()
    {
        if (currentIndex >= 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void DontDestroy()
    {
        if (Instance == null) {
            Instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    public void GoToNextLevel()
    {
        //Increasing scene index and going to the next scene
        currentIndex++;
        SceneManager.LoadScene(currentIndex);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex ) ;
    }
}
