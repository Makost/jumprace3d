﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ProgressManager : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;
    [SerializeField] private List<GameObject> platforms;
    private float currentProgress;

    public void IncreaseProgress(GameObject currentPlatform)
    {
        var progress = (float)Array.IndexOf(platforms.ToArray(), currentPlatform) / (platforms.Count-1);
        if (currentProgress < progress)
        {
            uiManager.IncreaseProgressBar(progress);
        }
        
    }
}
