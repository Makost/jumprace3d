﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    [SerializeField] private float sensitivity;
    [SerializeField] private Transform lightSource;
    [SerializeField] private Transform indicatorMask;
    private Vector3 _lastFrameTouchPosition;

    void Update()
    {
        //Rotating player by dragging
        Rotate();
    }

    private void Rotate()
    {
        if (!GetComponent<Bounce>().isWon)
        {
#if UNITY_EDITOR
       
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePosition = Input.mousePosition;
                if (mousePosition!=_lastFrameTouchPosition)
                {
                    var difference = _lastFrameTouchPosition.x - mousePosition.x;
                    var zRotation = difference * sensitivity * Time.deltaTime;
                    // zRotation
                    var rotationVector = new Vector3(0, zRotation, 0);
                    transform.Rotate(rotationVector);
                    //Rotating light source to make player always highlighted
                    lightSource.Rotate(rotationVector,Space.World);
                    indicatorMask.Rotate(rotationVector,Space.World);
                }
            }
            _lastFrameTouchPosition = Input.mousePosition;
#elif UNITY_ANDROID
        if (Input.touchCount>0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                var difference = _lastFrameTouchPosition.x-touch.position.x;
                var zRotation = difference * sensitivity * Time.deltaTime;
                var rotationVector = new Vector3(0, zRotation, 0);
                transform.Rotate(rotationVector);
                //Rotating light source to make player always highlighted
                 lightSource.Rotate(rotationVector,Space.World);
                 indicatorMask.Rotate(rotationVector,Space.World);
            }

            _lastFrameTouchPosition = touch.position;
        }
#endif 
        }

    }
}