﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
   [SerializeField] private float startTimer = 5f;

    private void Update()
    {
        DestroyTimer();
    }

    private void DestroyTimer()
    {
        startTimer -= Time.deltaTime;
        if (startTimer < 0)
        {
            Destroy(gameObject);
        }
    }
}
