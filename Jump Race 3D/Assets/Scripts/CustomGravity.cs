﻿using UnityEngine;

public class CustomGravity : MonoBehaviour
{
    public static float globalGravity = -9.81f;

    public float gravityScale = 1.0f;

    private Rigidbody m_rb;

    private void OnEnable()
    {
        m_rb = GetComponent<Rigidbody>();
        m_rb.useGravity = false;
    }

    private void FixedUpdate()
    {
        var gravity = globalGravity * gravityScale * Vector3.up;
        m_rb.AddForce(gravity, ForceMode.Acceleration);
    }
}