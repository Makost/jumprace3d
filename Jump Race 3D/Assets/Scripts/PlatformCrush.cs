﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformCrush : MonoBehaviour
{
    private float startTimer = 5f;
    void Start()
    {
        GetComponent<Rigidbody>().velocity=new Vector3(Random.Range(-3f,3f),Random.Range(-1f,1f),Random.Range(-3f,3f));
    }

    private void Update()
    {
        DestroyTimer();
    }

    private void DestroyTimer()
    {
        startTimer -= Time.deltaTime;
        if (startTimer < 0)
        {
            Destroy(gameObject);
        }
    }
}
