﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextEffectManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> textEffects;
    [SerializeField] private Transform canvas;
    public void ShowEffect(int index)
    {
        Instantiate(textEffects[index],canvas);
    }
}
